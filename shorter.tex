\documentclass[letterpaper]{article}

\usepackage[margin=1in]{geometry}

\usepackage{mathtools} % amsmath with fixes and additions
\usepackage{algorithm}
\usepackage{algorithmicx}
\usepackage[utf8]{inputenc} % allow utf-8 input
\usepackage[T1]{fontenc}    % use 8-bit T1 fonts
\usepackage{amsmath}       % hyperlinks
\usepackage{amsthm}       % hyperlinks
\usepackage{todo}
\usepackage{amssymb}       % hyperlinks
\usepackage{hyperref}       % hyperlinks
\usepackage{tikz}
\usepackage{tabu}
\usepackage{bbm}
\usepackage{url}            % simple URL typesetting
\usepackage{booktabs}       % professional-quality tables
\usepackage{amsfonts}       % blackboard math symbols
\usepackage{nicefrac}       % compact symbols for 1/2, etc.
\usepackage{subfig}
\usepackage{booktabs}
\usepackage{microtype}      % microtypography

\usepackage[utf8]{inputenc}
\usepackage[english]{babel}

\newtheorem{theorem}{Theorem}
\newtheorem{lemma}{Lemma}
\newtheorem{remark}{Remark}
\newtheorem{corollary}{Corollary}
\newtheorem{proposition}{Proposition}
\newtheorem{definition}{Definition}
\newtheorem{example}{Example}

\title{Formulating Identification Problems as Polynomial Programs in
  (Augmented) Latent Variable DAG models}
% \author{Noam Finkelstein}

\begin{document}
\maketitle

\section{Problem Statement}

Suppose we have a DAG with variables $V \equiv C \dot\cup U$, such that all
variables in $C$ have known finite cardinality, and all variables in $U$ have
unknown or infinite cardinality. By convention we use uppercase letters to
denote sets of random variables, and lowercase letters to denote singletons. We
do not use the usual distinction between observed and unobserved variables
because, as we will see below, it will be useful to reason about unobserved
random variables of known cardinality in the context of missing data and
measurement error.

The structural equations model of a DAG $\mathcal G$ assumes that each variable
$v \in V$ is a deterministic function of its parents in the graph,
denoted $pa_{\mathcal G}(v)$, and some relevant exogenous noise, denoted
$\epsilon_v$ \cite{pearl2009causality, richardson2013singlewi}. In other words,
the value of each $v \in V$ is given by $v = f_v(pa_{\mathcal G}(v),
\epsilon_v)$.

The random variable $Y(A = a)$ represents a variable that takes the distribution
$Y$ would take, were $A$ exogenously set to the value $a$, and is called a
\emph{counterfactual} random variable. By contrast a \emph{factual} random
variable takes the distribution it naturally takes without intervention on its
causes.

Under the structural equations model, the DAG represents a distribution over all
factual and counterfactual versions of $V$. We are particularly interested in
the distribution over all factual and counterfactual versions of $C$, and
specifically the counterfactual versions of $C$ after interventions on other
variables in $C$. We call the distribution over all such variables the
\emph{full data law with respect to C}. For the remainder of this work, we refer
to this simply as the full data law.

It has been observed that both factual and counterfactual random variables under
a structural equations model of a DAG are deterministic functions of the
collection of exogenous noise variables, $\epsilon_V$. One easy way to see that
this is the case is to take the task of each $\epsilon_v$ to be determining how
each random variable $v$ responds to different settings of its parents. In this
framework, for any particular setting of $\epsilon_V$, any factual variable of
interest can be obtained by first calculating the values of variables without
endogenous parents, and then iteratively calculating the values of variables
with parents who values are already known. Counterfactual random variables can
be similarly calculated, except that in the calculation of $Y(A = a)$, the
natural values of $A$ are irrelevant, and the functions that determine their
children are fed the values $a$ instead. We will make substantial use of the
idea that all factual and counterfactual random variables of interest are
deterministic functions of exogenous noise.

The task of causal inference in (and that of its cousin, inference under missing
data) can be rephrased as the task of \emph{bounding} functionals of the full
data law, subject to \emph{modeling constraints} and \emph{empirical
  constraints}. Modeling constraints represent \emph{a priori} assumptions made
by the analysts about the full data law. Empirical constraints come from data
collected on any conditional or marginal distributions of the full data law. In
this work, we show that when the model over the full data law is the structural
equations model of a DAG -- potentially in combination with other modeling
assumptions -- this bounding problem can be exactly represented by a polynomial
program.

\section{Preliminaries}

In this section, we will refer the reader to three important results that our
methods builds on.

First, we will discuss a way to simplify a DAG $\mathcal G$ without altering its
model of the full data law. It has been shown that for a DAG $\mathcal G$ with
$V \equiv C \dot\cup U$ has the same full data law as a DAG in which every
variable in $U$ is exogenous, and there is no pair of variables in $U$ such that
the children of one are a subset of a children of the other
\cite{evans2018margins}. To simplify our task, we therefore assume throughout
this work, without loss of generality, that the graph of interest has these
properties. A DAG with these properties is said to be in \emph{canonical} form.
A guide to converting arbitrary DAGs into this form is provided in Appendix
\ref{app:canonical}.

Next, we make use of a result that deterministically relates the variables of
interest $C$ to the variables of unknown cardinality $U$. Starting with such a
DAG, we can obtain a \emph{non-restrictive functional model} of $C$ in terms of
$U$ according to an algorithm presented in \cite{wolfe2021cardinalities}. This
functional model allows us to assume without altering the distribution over
factual values of $C$ that $U$ take values in finite state spaces, and that
\emph{factual} values of $C$ are deterministic functions of $U$. We denote this
functional model $\mathcal F_{\mathcal G}(C)$. In this work, we make use of a
minor extension of this result, provided below. Proofs are deferred to the
appendix.

\begin{proposition} \label{prop:functional-model}
  Suppose $\mathcal G$ is a DAG over variables $V \equiv C \dot\cup U$ such that
  each variable in $U$ is exogenous, and there is no pair of variables in $U$
  such that the children of one are a subset of the children of the other. Then
  the functional model proposed in \cite{wolfe2021cardinalities} for $C$
  in terms of $U$ is equal to the structural equations model over the full data
  law, and all factual \emph{and counterfactual} versions of $C$ are
  deterministic functions of $U$.
\end{proposition}

Finally, we will make use of a concise parameterization of \emph{single world}
margins of the full data law. A single-world margin involves only random
variables under the same intervention, i.e. it can be written $P(Y(A = a))$ for
some collections of variables $Y$ and $A$. The structural equation model places
a number of equality constraints on such distributions, each of which can be
used to eliminate a parameter of the distribution relative to the na\"ive
discrete parameterization. The \emph{nested markov parameterization}
\cite{evans2019smooth} achieves a number of parameters equal to the dimension of
the model when there are no known deterministic relationships between variables
in $C$. Even in the presence of such relationships, which sometimes occur in the
context of missing data, we take advantage of this parameterization to enforce
empirical constraints on the estimand with a reduced number of constraints, and
fewer parameters and lower degree polynomials in each constraint.

\section{Formulating the Polynomial Program}

In this section, we will show how many problems in causal inference and missing
data can be sharply bounded by solving a polynomial program. Our strategy will
be to take advantage of the functional model $\mathcal F_{\mathcal G}(C)$ to
limit the parameters of the program to the parameters of the distribution on
$U$. Because all versions of $C$ are functions of $U$, it will always be
sufficient to consider values of $U$ that lead to the relevant outcomes in
versions of $C$.

We begin by considering marginal \emph{probabilities} of the full data law.
These can without loss of generality be represented as $P(c_1(A_1 = a_1) =
x_{c_1}, \dots, c_n(A_n = a_n) = x_{c_n})$, with $c_i \in C$ and $A_i \subset C$
for all $i$. Recalling that we can assume without loss of generality that all
variables in $U$ are exogenous, and therefore marginally independent of each
other, it follows that $P(U = x_U) = \prod_{u \in U} P(u = x_u)$, where $x_u$
indicates the value that $u$ takes in $x_U$.

By Proposition \ref{prop:functional-model}, all factual and counterfactual
versions of $C$ can, again without loss of generality, be assumed to be
deterministic functions of $U$ under the functional model $\mathcal F_{\mathcal
  G}(C)$. Therefore the probability $P(c_1(A_1 = a_1) = x_{c_1}, \dots, c_n(A_n
= a_n) = x_{c_n})$ can be calculated as the probability that $U$ take values
leading to $c_i(A_i = a_i) = x_{v_i}$ for each $i$.

These results together lead to the following proposition.

\begin{proposition} \label{prop:polynomialization}
  Suppose $\mathcal G$ is in its canonical form, and let $\{U = x_U \implies
  c_1(A_1 = a_1) = x_{c_1}, \dots, c_n(A_n = a_n) = x_{c_n}\}$ indicate the set
  of values of $U$ that lead deterministically to $c_1(A_1 = a_1) = x_{c_1},
  \dots, c_n(A_n = a_n) = x_{c_n}$ under $\mathcal F_{\mathcal G}(C)$. Then
  under the structural equations model of $\mathcal G$,
\begin{align*}\label{eq:polynomial}
P(c_1(A_1 = a_1) = x_{c_1}, \dots, c_n(A_n = a_n) = x_{c_n})
= \sum_{\{{U = x_U} \implies c_1(A_1 = a_1) = x_{c_1}, \dots, c_n(A_n = a_n) = x_{c_n}\}}
\prod_{u \in U}P(u = x_u).
\end{align*}
\end{proposition}

We now expand this result to include a large class of functionals of marginal
probabilities.

\begin{corollary} \label{cor:functional-polynomialization}
  Suppose $\mathcal G$ is in its canonical form, let $P_F$ denote the full data
  law, and $g(P_F)$ denote a functional of $P_F$ involving simple arithmetic
  operations on marginal probabilities of $P_F$, denoted and constants. Then
  $g(P_F)$ can be expressed as a polynomial fraction in the parameters of $P(U)$
  by replacing each marginal probability by its polynomialization from
  Proposition \ref{prop:polynomialization}.
\end{corollary}

This corollary demonstrates that a wide array of single world and cross world
functionals can be expressed as polynomial fractions in the parameters of
$P(U)$. These include traditional targets such as the ACE, as well as more
complex targets, such as the pure direct effect and the probability of causal
sufficiency. We say functionals of the full data that fulfill the properties
described in the corollary are \emph{polynomializable}. 

We can now propose our simplest algorithm for constructing a polynomial
program to sharply bound factual and counterfactual targets. We let $\mathcal G$
denote a canonical form DAG, $\mathcal T$ denote a polynomializable target of
inference, and $\mathcal E$ and $\mathcal A$ denote the sets of constraints on
polynomializable functionals resulting from empirical evidence and by modeling
assumptions respectively. Finally, let $\bf \star$ denote any binary comparison
operator, $<, \le, >, \ge, =, \neq$. 

\begin{algorithm}[h]
	\caption{Constructing a Polynomial Program}
  \label{alg:polynomial-program}
	\begin{algorithmic}[1]
		\Statex \hspace{-6.5mm} \textbf{Input:} $\mathcal G$, 
    $\mathcal E$, $\mathcal A$, $\mathcal T$
		\Statex \hspace{-6.5mm} \textbf{Output:} polynomial program
    \State $\mathcal F_{\mathcal G}(C) \leftarrow$ find functional model of $C$
    in $U$ for $\mathcal G$
    \State $\mathcal T \leftarrow$ polynomialize $\mathcal T$ in $\mathcal
    F_{\mathcal G}(C)$
    \State $\mathcal C \leftarrow \emptyset$
    \State \textbf{For} $g(P_F) ~{\bf \star}~ p
    \in (\mathcal E \cup \mathcal A)$:
    \State \hspace{5mm} $\mathcal C.add$($p~\bf \star$ polynomialze $g(P_F)$ in $\mathcal F_{\mathcal G}(C)$) 
    \State \textbf{For} $u \in U$:
    \State \hspace{5mm} $\mathcal C.add$($P(u)$ is a distribution)
    \State \textbf{return} optimize $\mathcal T$ subject to $\mathcal C$
	\end{algorithmic}
\end{algorithm}

\begin{theorem}
  Minimization (maximization) of the polynomial program produced by Algorithm
  \ref{alg:polynomial-program} produces sharp lower (upper) bounds on the
  $\mathcal T$ under the structural equation model $\mathcal G$, additional
  modeling assumptions $\mathcal A$, and empirical evidence $\mathcal E$.
\end{theorem}

\section{Methods for Reducing The Complexity of the Polynomial Program}

Solving polynomial programs is NP hard, so it is important to take full
advantage of our knowledge of the structure of the problem to reduce the
complexity of the program. In this section, we discuss several ways to do this.

We begin by using the following observation to limit the number of latent
variable distributions involved in each constraint.

\begin{proposition} \label{prop:relevant-latents}
  The polynomialization of a probability $P(c_1(A_1 = a_1) = x_{c_1}, \dots,
  c_n(A_n = a_n) = x_{c_n})$ can be expressed in terms of the parameters of only
  latent variables from which there is a path to $c_i$ not through $A_i$ for
  some $i$.
\end{proposition} 

In other words, each $c_i$ is only a function of its ancestors in $U$ that
affect it through a variable not under intervention. For each marginal
probability, the distributions of latent variables of which no variable in the
event is a function are irrelevant. The polynomialization of Proposition
\ref{prop:polynomialization} can therefore be amended such that the outer sum
ranges only over all possible settings of \emph{relevant} latent variables,
reducing the degree of each term in the polynomial. In the following sections,
we provide two less straight-forward methods for simplifying the program.

\subsection{Using the Nested Markov Parameterization for Single-World Marginal Distributions}

In many cases, the empirical evidence analysts have access to include
\emph{single world marginal distributions}, i.e. the probability $P(c_1(A = a) =
x_{c_1}, \dots, c_n(A = a) = x_{c_n})$ for every event in the state space
$\times_{i \in \{1, \dots, n\}} \mathcal X_{c_i}$. This includes the factual
distribution over any subset of $C$, for which $A = \emptyset$, as well as
experimental data.

The na\"ive approach to polynomializing this distribution would be to
polynomialize every probability in the state space, and add the resulting
polynomial to the program constraints. An immediate, minor improvement would be
to not add one of the constraints constructed in this way, as it is already
implied by the fact that all distributions must marginalize to unity. In
general, we can reduce the number of constraints we add to the program whenever
\emph{other} equality constraints apply to the distribution $P(c_1(A = a) =
x_{c_1}, \dots, c_n(A = a) = x_{c_n})$ as well.

The equality constraints imposed by the structural equations model of $\mathcal
G$ on such single world margins are well understood. In particular, it places
certain \emph{conditional independence} and \emph{generalized equality
  constraints}, also known as Verma constraints, on these distributions. Each of
these equality constraints can be used to reduce the number of parameters needed
to fully describe the distribution $P(c_1(A = a) = x_{c_1}, \dots, c_n(A = a) =
x_{c_n})$ by one. The parameterization that takes full advantage of these
equality constraints to maximally reduce the number of parameters is called the
\emph{nested markov parameterization}, described briefly above.

Each nested markov parameter is exactly equal to a marginal \emph{probability}
of a single-world event -- which is not, in general, in the same single world as
the original distribution -- and as such is a polynomial function of the
parameters of the distributions of $U$, as described in Proposition
\ref{prop:polynomialization}. In addition, each of the nested markov parameters
is identified from the initial single-world distribution $P(c_1(A = a) =
x_{c_1}, \dots, c_n(A = a) = x_{c_n})$. This allows us to add one constraint per
nested markov parameter, with the corresponding polynomial set equal to its
value, as calculated from the original single world distribution. Rather than
adding equality constraints na\"ively for every outcome in the state space, we
can add a smaller number of equality constraints corresponding to the number of
nested markov parameters.

We observe, in addition, that each parameter in the parameterization is a single
world event involving \emph{fewer} random variables and \emph{more} fixed
variables than the original single world distribution. This leads to constraints
with polynomials of lower degree by Proposition \ref{prop:relevant-latents}.
In some cases, the savings can be dramatic. An example is provided in Appendix
\ref{app:example}. Using this technique, we modify Algorithm
\ref{alg:polynomial-program} as follows. We now let $\mathcal M$ represent all
single-world marginal distributions, and $\mathcal E$ represent all
\emph{remaining} empirical evidence. The remaining inputs are as before.

\begin{algorithm}[]
	\caption{Constructing a Polynomial Program}
  \label{alg:polynomial-program}
	\begin{algorithmic}[1]
		\Statex \hspace{-6.5mm} \textbf{Input:} $\mathcal G$, $\mathcal M$,
    $\mathcal E$, $\mathcal A$, $\mathcal T$
		\Statex \hspace{-6.5mm} \textbf{Output:} polynomial program
    \State $\mathcal F_{\mathcal G}(C) \leftarrow$ find functional model of $C$
    in $U$ for $\mathcal G$
    \State $\mathcal T \leftarrow$ polynomialize $\mathcal T$ in $\mathcal
    F_{\mathcal G}(C)$
    \State $\mathcal C \leftarrow \emptyset$
    \State \textbf{For} $P(c_1({\bf a}), \dots, c_M({\bf a})) \in \mathcal M:$
    \State \hspace{5mm} $\mathcal P \leftarrow$ nested markov parameterization
    of $P(c_1({\bf a}), \dots, c_M({\bf a}))$
    \State \hspace{5mm} \textbf{For} $g(P_F) = p \in \mathcal P$:
    \State \hspace{10mm} $\mathcal C.add$($p =$ polynomialze $g(P_F)$ in
    $\mathcal F_{\mathcal G}(C)$)
    \State \textbf{For} $g(P_F) ~{\bf \star}~ p
    \in (\mathcal E \cup \mathcal A)$:
    \State \hspace{5mm} $\mathcal C.add$($p~\bf \star$ polynomialze $g(P_F)$ in $\mathcal F_{\mathcal G}(C)$) 
    \State \textbf{For} $u \in U$:
    \State \hspace{5mm} $\mathcal C.add$($P(u)$ is a distribution)
    \State \textbf{return} optimize $\mathcal T$ subject to $\mathcal C$
		\end{algorithmic}
\end{algorithm}

The proof of this algorithm's validity is nearly identical to the earlier proof.
In general, we say that two parameters \emph{co-occur} in a polynomial system if
they appear in the same polynomial constraint. We say that they \emph{interact}
if there exists an ordered sequence of parameters beginning with one and ending
with the other such that every adjacent pair of parameters in the sequence
co-occur. If a constraint involves only parameters that do not interact with any
of the parameters in the objective, that constraint may be dropped without
altering the program. If parameters in the system exist only in constraints that
have been dropped from the program, then they have also (trivially) exited the
program, and the number of parameters has been decreased.

We conclude this section with an observation about how this approach may
simplify the program. Observing that each nested markov parameter corresponds to
a single world event involving versions of random variables in a single
district, we present the following proposition.

\begin{proposition}
  The degree of each polynomial in the constraints induced by the nested markov
  parameterization of a single world marginal distribution is bounded from above
  by the number of latent variables in the corresponding district. Moreover, no
  parameters of the distributions of latent variables that are not in the same
  district interact in these constraints.
\end{proposition}
% \begin{proof}

% \end{proof}

A consequence of this proposition is that in the common scenario where we
observe the full joint distribution over factual versions of $C$ and aim to
bound a functional relating the response of some outcome to treatment in the
same district, we can drop from the program all constraints related to nested
markov parameters involving other districts, and therefore all parameters of the
distributions of variables in $U$ that are in other districts.

For a complete understanding of how to obtain the nested markov parameterization
of an arbitrary single world distribution, we refer the reader to
\cite{evans2019smooth}. We note that when certain deterministic
relationships exist between variables in $C$, these relationships may imply
equality constraints not capitalized on by the nested markov parameterization.
This situation arises in some missing data settings, where the observed proxy
variable is a deterministic function of the underlying truth and the censoring
variable. It may also arise in other settings, where there exist deterministic
relationships between domain variables. In such cases, it may be possible to
further reduce the number of constraints in the program, but we do not explore
that option here.

\subsection{DAG parameterization for Non-geared graphs}

Most graphs we encounter in practice are \emph{geared} \cite{evans2018margins},
which means they have no \emph{non-trivial bi-directed cycles}
\cite{wolfe2021cardinalities}. When graphs are not geared, and the target
estimand as well as all empirical evidence involves only single world
probabilities, it is possible to improve the complexity of the system. Under
these circumstances, it is preferable to obtain non-restrictive bounds on the
cardinalities of latent variables according to \cite{wolfe2021cardinalities}.
All single world probabilities can be expressed in terms of the usual DAG
parameters according to the G-Formula, and therefore all functionals of such
probabilities described in Corollary \ref{cor:functional-polynomialization} can
be polynomialized as well. If the target or any of the empirical
evidence involve cross-world probabilities, we must revert to the functional
model approach.

\section{Missing Data and Measurement Error}

In the presence of measurement error, we suppose one variable in $C$ is the
unobserved truth of interest, and one or more variables also in $C$ serve as its
proxy. These variables can also have causal connections to other variables in
the graph. In the case of missing data, we additionally have a censoring
variable, also in $C$, that has a \emph{deterministic} relationship with the
observed proxy -- when the censoring variable equals $0$, we have a missing
observation, and the observed proxy is set equal to the special value denoted by
?.  In traditional approaches to missing data, when the censoring variable is
equal to $1$, it is assumed that the observed proxy is exactly equal to the
unobserved truth. One of the benefits of our approach is that we can easily
allow the observed proxy to be measured with error, whether or not it is also
subject to missingness.

The deterministic relationships present in missing data graphs affect the
functional model $\mathcal F_{\mathcal G}(C)$, in that some functions for the
observed proxy become \emph{impossible}. In Appendix
\ref{app:functional-models}, we describe an adapted procedure for incorporating
known deterministic relationships between variables in $C$ into the functional
model $\mathcal F_{\mathcal G}(C)$. It can be used for missing data graphs, as
well as in other scenarios with deterministic relationships.

\bibliographystyle{plain}
\bibliography{references}

\end{document}
