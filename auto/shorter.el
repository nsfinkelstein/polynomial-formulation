(TeX-add-style-hook
 "shorter"
 (lambda ()
   (setq TeX-command-extra-options
         "-shell-escape")
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("article" "letterpaper")))
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("geometry" "margin=1in") ("inputenc" "utf8") ("fontenc" "T1") ("babel" "english")))
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "path")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "url")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "nolinkurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperbaseurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperimage")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperref")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "href")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "path")
   (TeX-run-style-hooks
    "latex2e"
    "article"
    "art10"
    "geometry"
    "mathtools"
    "algorithm"
    "algorithmicx"
    "inputenc"
    "fontenc"
    "amsmath"
    "amsthm"
    "todo"
    "amssymb"
    "hyperref"
    "tikz"
    "tabu"
    "bbm"
    "url"
    "booktabs"
    "amsfonts"
    "nicefrac"
    "subfig"
    "microtype"
    "babel")
   (LaTeX-add-labels
    "prop:functional-model"
    "prop:polynomialization"
    "eq:polynomial"
    "cor:functional-polynomialization"
    "alg:polynomial-program"
    "prop:relevant-latents")
   (LaTeX-add-bibliographies
    "references")
   (LaTeX-add-amsthm-newtheorems
    "theorem"
    "lemma"
    "remark"
    "corollary"
    "proposition"
    "definition"
    "example"))
 :latex)

